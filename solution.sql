-- S06 - Activity-B

 /*

	A. List the books authored by Marjorie Green.
	- The Busy Executive's Database Guide
	- You Can Combat Computer Stress!

	B. List the books authored by Michael O'Leary.
	- Cooking with Computers
	- TC7777 (not found in the given Database)

	C. Write the author/s of "The Busy Executives Database Guide".
	- Marjorie Green
	-	Abraham Bennet

	D. Identify the publisher of "But Is It User Friendly?".
	- Algodata Infosystems

	E. List the books published by Algodata Infosystems.
	- The Busy Executive's Database Guide
	- Cooking with Computers
	- Straight Talk About Computers
	- But Is It User Friendly?
	- Secrets of Silicon Valley
	- Net Etiquette

	*/


-- blog_db

-- Create database
	CREATE DATABASE blog_db;

-- Select blog_db database
	USE blog_db;

-- Create users table
	CREATE TABLE users(
		id INT NOT NULL AUTO_INCREMENT,
		email VARCHAR(100) NOT NULL,
		password VARCHAR(300) NOT NULL,
		datetime_created DATETIME,
		PRIMARY KEY(id)
	);

-- Create posts table
	CREATE TABLE posts(
		id INT NOT NULL AUTO_INCREMENT,
		author_id INT NOT NULL,
		title VARCHAR(500),
		content VARCHAR(5000),
		datetime_posted DATETIME,
		PRIMARY KEY(id),
		CONSTRAINT fk_posts_author_id
			FOREIGN KEY(author_id)
			REFERENCES users(id)
			ON UPDATE CASCADE
			ON DELETE RESTRICT
	);

-- Create post_likes table
	CREATE TABLE post_likes(
		id INT NOT NULL AUTO_INCREMENT,
		post_id INT NOT NULL,
		user_id INT NOT NULL,
		datetime_liked DATETIME,
		PRIMARY KEY(id),
		CONSTRAINT fk_post_likes_post
			FOREIGN KEY(post_id)
			REFERENCES posts(id)
			ON UPDATE CASCADE
			ON DELETE RESTRICT,
		CONSTRAINT fk_post_likes_user
			FOREIGN KEY(user_id)
			REFERENCES users(id)
			ON UPDATE CASCADE
			ON DELETE RESTRICT
	);

-- Create post_comments table
	CREATE TABLE post_comments(
		id INT NOT NULL AUTO_INCREMENT,
		post_id INT NOT NULL,
		user_id INT NOT NULL,
		content VARCHAR(5000),
		datetime_commented DATETIME,
		PRIMARY KEY(id),
		CONSTRAINT fk_post_comments_post
			FOREIGN KEY(post_id)
			REFERENCES posts(id)
			ON UPDATE CASCADE
			ON DELETE RESTRICT,
		CONSTRAINT fk_post_comments_user
			FOREIGN KEY(user_id)
			REFERENCES users(id)
			ON UPDATE CASCADE
			ON DELETE RESTRICT
	);